/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __ESVG_TEXT_H__
#define __ESVG_TEXT_H__

#include <esvg/Base.h>

namespace esvg {
	class Text : public esvg::Base {
		public:
			Text(PaintState _parentPaintState);
			~Text();
			virtual bool parse(const std::shared_ptr<exml::Element>& _element, mat2& _parentTrans, vec2& _sizeMax);
			virtual void display(int32_t _spacing);
	};
};

#endif

