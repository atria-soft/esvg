/**
 * @author Edouard DUPIN
 * 
 * @copyright 2014, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include <etk/types.h>

#ifndef __ESVG_TEST_MAIN_H__
#define __ESVG_TEST_MAIN_H__

extern bool g_visualDebug;


#endif
